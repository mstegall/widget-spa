import React, { Component } from "react";

import { fetchUsers } from "../services/api";

import Table from "../components/usersList/tableContainer";

export default class UsersList extends Component {
    state = {
        users: [],
    };

    componentDidMount() {
        this.loadUsers();
    }

    loadUsers() {
        fetchUsers().then(users => this.setState({ users }));
    }

    render() {
        return <Table users={this.state.users} />;
    }
}
