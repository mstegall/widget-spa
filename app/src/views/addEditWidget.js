import React, { Component } from "react";

import { updateWidget, fetchWidget, addWidget } from "../services/api";

import Form from "../components/addEditWidget/formContainer";

export default class AddEditWidget extends Component {
    state = {
        widget: {},
    };

    componentDidMount() {
        if (this.props.id) {
            this.loadWidget(this.props.id);
        }
    }

    loadWidget(id) {
        fetchWidget(id).then(data => this.setState({ widget: data }));
    }

    updateWidget = data => {
        updateWidget(data).then(() => {
            window.history.back();
        });
    };

    addWidget(data) {
        addWidget(data).then(() => {
            window.history.back();
        });
    }

    onSubmit = data => {
        if (this.props.id) {
            return this.updateWidget(data);
        }
        return this.addWidget(data);
    };

    render() {
        return <Form widget={this.state.widget} onSubmit={this.onSubmit} />;
    }
}
