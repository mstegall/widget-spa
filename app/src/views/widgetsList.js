import React, { Component } from "react";

import { fetchWidgets } from "../services/api";

import Table from "../components/widgetsList/tableContainer";

export default class WidgetsList extends Component {
    state = {
        widgets: [],
    };

    componentDidMount() {
        this.loadWidgets();
    }

    loadWidgets() {
        fetchWidgets().then(widgets => this.setState({ widgets }));
    }

    render() {
        return <Table widgets={this.state.widgets} />;
    }
}
