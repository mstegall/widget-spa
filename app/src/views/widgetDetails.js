import React, { Component } from "react";

import { fetchWidget } from "../services/api";

import Details from "../components/widgetDetails/details";

export default class WidgetDetails extends Component {
    state = {
        widget: {},
    };

    componentDidMount() {
        this.loadWidget(this.props.id);
    }

    loadWidget(id) {
        fetchWidget(id).then(widget => this.setState({ widget }));
    }

    render() {
        return <Details widget={this.state.widget} />;
    }
}
