import React, { Component } from "react";

import { fetchUsers, fetchWidgets } from "../services/api";

import Boxes from "../components/dashboard/boxes";
import Box from "../components/dashboard/box";
import Tables from "../components/dashboard/tables";
import UsersTable from "../components/dashboard/usersTableContainer";
import WidgetsTable from "../components/dashboard/widgetsTableContainer";

export default class Dashboard extends Component {
    state = {
        users: [],
        widgets: [],
    };

    componentDidMount() {
        this.loadUsers();
        this.loadWidgets();
    }

    loadUsers() {
        fetchUsers().then(users => this.setState({ users }));
    }

    loadWidgets() {
        fetchWidgets().then(widgets => this.setState({ widgets }));
    }

    render() {
        const { users, widgets } = this.state;

        return (
            <div>
                <Boxes>
                    <Box title={users.length} comment="Users" />
                    <Box title={widgets.length} comment="Widgets" />
                </Boxes>
                <Tables>
                    <UsersTable users={users} />
                    <WidgetsTable widgets={widgets} />
                </Tables>
            </div>
        );
    }
}
