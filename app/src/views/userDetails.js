import React, { Component } from "react";

import { fetchUser } from "../services/api";

import Details from "../components/userDetails/details";

export default class UsersList extends Component {
    state = {
        user: {},
    };

    componentDidMount() {
        this.loadUser(this.props.id);
    }

    loadUser(id) {
        fetchUser(id).then(user => this.setState({ user }));
    }

    render() {
        return <Details user={this.state.user} />;
    }
}
