import React, { Component } from "react";

import Form from "./form";

export default class FormContainer extends Component {
    state = {
        name: "",
        color: "",
        price: "",
        melts: false,
        inventory: "",
    };

    componentWillReceiveProps({ widget }) {
        this.setState(widget);
    }

    onChange = event => {
        const { value, name } = event.target;

        this.setState({
            [name]: value,
        });
    };

    onChangeCheck = event => {
        const { name } = event.target;

        this.setState(prevState => ({
            [name]: !prevState[name],
        }));
    };

    onSubmit = event => {
        event.preventDefault();
        this.props.onSubmit(this.state);
    };

    render() {
        return (
            <Form
                data={this.state}
                onChange={this.onChange}
                onCheckChange={this.onChangeCheck}
                onSubmit={this.onSubmit}
            />
        );
    }
}
