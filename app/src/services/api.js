export default function callApi(path, options = {}) {
    const defaultOptions = {
        mode: "cors",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
        },
    };

    return (
        fetch(
            `http://spa.tglrw.com:4000/${path}`,
            Object.assign(defaultOptions, options)
        )
            // parse response if present
            .then(response => {
                const { method } = options;
                if (!method || method === "GET") {
                    return response.json();
                }

                return response;
            })
    );
}

export function fetchWidget(id) {
    return callApi(`widgets/${id}`);
}

export function fetchWidgets() {
    return callApi("widgets");
}

export function updateWidget(data) {
    const options = {
        method: "PUT",
        body: JSON.stringify(data),
    };

    return callApi(`widgets/${data.id}`, options);
}

export function addWidget(data) {
    const options = {
        method: "POST",
        body: JSON.stringify(data),
    };

    return callApi("widgets", options);
}

export function fetchUser(id) {
    return callApi(`users/${id}`);
}

export function fetchUsers() {
    return callApi(`users`);
}
