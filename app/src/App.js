import React, { Component } from 'react';

import { BrowserRouter, Route } from "react-router-dom";

import Sidebar from "./components/sidebar";
import Header from "./components/header";

import Dashboard from "./views/dashboard";
import Users from "./views/usersList";
import UserDetail from "./views/userDetails";
import Widgets from "./views/widgetsList";
import WidgetDetail from "./views/widgetDetails";
import AddEditWidget from "./views/addEditWidget";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div id="page-wrapper" className="open">
          <Sidebar />
          <div id="content-wrapper">
            <div className="page-content">
              <Header />
              
              <Route exact path="/" component={Dashboard} />
              <Route exact path="/user" component={Users} />
              <Route path="/user/:id" render={props => <UserDetail id={props.match.params.id}/>} />
              <Route exact path="/widget" component={Widgets} />
              <Route path="/widget/add" component={AddEditWidget} />
              <Route path="/widget/update/:id" render={props => <AddEditWidget id={props.match.params.id}/>} />
              <Route exact path="/widget/details/:id" render={props => <WidgetDetail id={props.match.params.id}/>} />
            </div>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
